import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "../../core/base.component";
import {BaseService} from "../../core/base.service";

@Component({
    selector: 'app-cms',
    templateUrl: './cms.component.html',
    styleUrls: ['./cms.component.css']
})
export class CmsComponent extends BaseComponent implements OnInit {

	new_page:any = false;
	current_block:any = null;
	new_block:any = false;

    constructor(protected baseService: BaseService) {
        super(baseService);
    }

    ngOnInit() {
        this.baseService.methodGet('cms/listpage').subscribe(res => {
            let rs: any = res;
            this.list_element = rs.data;
            if(this.list_element.length > 0){
            	this.current_edit = this.list_element[0];
            	if(this.current_edit.blocks && this.current_edit.blocks.length > 0) {
            		this.current_block = this.current_edit.blocks[0];
            	}
            }
        });
    }

    editPage(item) {
    	this.editElement(item);
    	this.new_page = false;
    	this.current_block = null;

    	if(item.blocks && item.blocks.length > 0) {
    		this.current_block = item.blocks[0];
    	}
    }

    savePage(item) {
        this.saveElement(item, 'page');
    }

    deletePage(item) {
        this.deleteElement(item, 'page');
    }

    createPage() {
    	this.new_page = true;
    	this.current_edit = {};
    	this.current_block = null;
    }

    saveNewPage(item) {
    	this.new_page = false;
    	this.createElement(item, 'page');
    }

    editBlock(item) {
    	this.new_block = false;
    	this.current_block = item;
    }

    createBlock() {
    	this.new_block = true;
    	this.current_block = {};
    }

    saveBlock(item) {
    	this.saveElement(item, 'block');
    }

    deleteBlock(item) {
    	this.deleteElement(item, 'block');
    	if(this.current_edit.blocks && this.current_edit.blocks.length > 0) {
    		this.current_block = this.current_edit.blocks[0];
    	}
    }

    createNewBlock(item, page_id) {
    	this.new_block = false;
    	item.page_id = page_id;
    	this.createElement(item, 'block');
    }

    addPost(post_id, block_id) {
    	let fd = new FormData();
    	fd.append('post_id', post_id);
    	fd.append('block_id', block_id);
    	var self = this;
    	this.baseService.methodPost('block/addpost', fd).subscribe(
    	    res => {
    	        let rs: any = res;
    	        if(rs.success){
    	        	if(!self.current_block.posts){
    	        		self.current_block.posts = [];
    	        	}
    	           	self.current_block.posts.push(rs.data);
    	        }
    	    },
    	    error => {
    	        alert('loi roi');
    	    }
    	);
    }

    removePost(post_id, block_id) {
    	let fd = new FormData();
    	fd.append('post_id', post_id);
    	fd.append('block_id', block_id);
    	var self = this;
    	this.baseService.methodPost('block/removepost', fd).subscribe(
    	    res => {
    	        let rs: any = res;
    	        if(rs.success){
    	           	self.current_block.posts = self.current_block.posts.filter(function(el){ return el.id != post_id; });
    	        }
    	    },
    	    error => {
    	        alert('loi roi');
    	    }
    	);
    }
}

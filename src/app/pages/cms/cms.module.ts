import {NgModule} from '@angular/core';
import {Routes, RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {CommonModule} from '@angular/common';
import {CmsComponent} from './cms.component';

//auth
import { AuthGuard } from '../../_guards/auth.guard';

export const routes: Routes = [
    {
        path: 'cms',
        component: CmsComponent,
        children: [
            {
                path: '', component: CmsComponent,
            }
        ],
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [CmsComponent]
})
export class CmsModule {
}

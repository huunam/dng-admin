import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from "@angular/router";
import {CategoryComponent} from './category.component';
import {CategoryCreateComponent} from './category-create/category-create.component';
import {CategoryListComponent} from './category-list/category-list.component';

//auth
import { AuthGuard } from '../../_guards/auth.guard';

export const routes: Routes = [
    {
        path: 'category',
        component: CategoryComponent,
        children: [
            {
                path: '', component: CategoryListComponent,
            },
            {
                path: 'create', component: CategoryCreateComponent
            }
        ],
        canActivate: [AuthGuard]
    }

];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [CategoryComponent, CategoryCreateComponent, CategoryListComponent]
})
export class CategoryModule {
}

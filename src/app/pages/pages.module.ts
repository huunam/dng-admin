import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TesteditorModule} from './testeditor/testeditor.module';
import {CategoryModule} from './category/category.module';
import {PostModule} from './post/post.module';
import {CategoryService} from "./category/category.service";
import {PostService} from "./post/post.service";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {CmsModule} from './cms/cms.module';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        // TesteditorModule,
        CategoryModule,
        PostModule,
        CmsModule,
        FormsModule
    ],
    declarations: [],
    providers: [
        CategoryService,
        PostService
    ]
})
export class PagesModule {
}

import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/first';

import { User } from '../_models/user';
import { UserService } from '../_services/user.service';
import { AuthenticationService } from '../_services/authentication.service';

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {
    users: User[] = [];

    constructor(private userService: UserService, private authenticationService: AuthenticationService) { }

    ngOnInit() {
        // get users from secure api end point
        this.userService.getAll()
            .first()
            .subscribe(users => {
                this.users = users;
            });
    }

    logout() {
        this.authenticationService.logout();
        window.location.reload();
    }

}
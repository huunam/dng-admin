import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from "@angular/router";
import {PostComponent} from './post.component';
import {PostListComponent} from './post-list/post-list.component';
//wysiwyg editor
import {FroalaEditorModule, FroalaViewModule} from 'angular-froala-wysiwyg';
import { PostCreateComponent } from './post-create/post-create.component';

//auth
import { AuthGuard } from '../../_guards/auth.guard';

export const routes: Routes = [
    {
        path: 'post',
        component: PostComponent,
        children: [
            {
                path: '', component: PostListComponent,
            },
            {
                path: 'create', component: PostCreateComponent
            }
        ],
        canActivate: [AuthGuard] 
    }
];

@NgModule({
  imports: [
    FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
    CommonModule,
    BrowserModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PostComponent, PostListComponent, PostCreateComponent]
})
export class PostModule { }

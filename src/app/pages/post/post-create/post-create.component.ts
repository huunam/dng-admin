import { Component, OnInit } from '@angular/core';
import {BaseService} from "../../../core/base.service";
import {Router} from '@angular/router';

import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';

declare var $: any;
declare var swal: any;

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit {
    	
	data: any = {};
	all_category: any;

    //search tag
    result_search_tag: any = [];
    show_box_search_tag: any = false;
    keyword_tag: any;
    modelChanged: Subject<string> = new Subject<string>();

    public options: Object = {
        placeholderText: 'Edit Your Content Hwwwwwwwwwwwere!',
        charCounterCount: true,
        heightMax: 500
    };
    
	constructor(protected baseService: BaseService, private router: Router) { 

        this.modelChanged
            .debounceTime(400) // wait 400ms after the last event before emitting last event
            .distinctUntilChanged() // only emit if value is different from previous value
            .subscribe(keyword => {
                this.keyword_tag = keyword;
                if(this.keyword_tag){
                    this.searchTag(this.keyword_tag);
                }else{
                    this.show_box_search_tag = false;
                }
            });
    }

    changed(text: string) {
        this.modelChanged.next(text);
    }

	ngOnInit() {
        $('.dropify').dropify();
		this.baseService.methodGet('category/list').subscribe(res => {
            let rs: any = res;
            this.all_category = rs.data;
        });
	}

    create() {
        var self = this;
        
        var fd = new FormData($('#post_form')[0]);
        this.baseService.methodPost('post/create', fd).subscribe(
            res => {
                let rs: any = res;
                if(rs.success){
                    swal({
                        title: "GOod JObs!",
                        text: "Create Success",
                        type: "success"
                    }, function() {
                        self.router.navigate(['/post'])
                    });
                }else{
                    swal('ERorR!', 'Fails', 'danger');
                }
            },
            error => {
                alert('loi roi');
            }
        )
        
    }

    searchTag(keyword) {

        this.baseService.methodGet('tag/search?keyword=' + keyword).subscribe(res => {
            let rs: any = res;
            this.result_search_tag = rs.data;

            if(this.result_search_tag.length > 0){
                this.show_box_search_tag = true;
            }else{
                this.show_box_search_tag = false;
            }
        });
    }

}

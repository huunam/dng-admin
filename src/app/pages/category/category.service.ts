/**
 * Created by phnam on 1/7/18.
 */
import {Injectable} from "@angular/core";
import {BaseService} from "../../core/base.service";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class CategoryService extends BaseService {
    constructor(http: HttpClient) {
        super(http);
    }

    listCategory(){
    	return this.methodGet('category/list');
    }

}
import {Component, OnInit} from '@angular/core';
import {CategoryService} from '../category.service';
import {BaseService} from "../../../core/base.service";
import {BaseComponent} from "../../../core/base.component";
import {Router} from '@angular/router';

declare var $: any;
declare var swal: any;

@Component({
    selector: 'app-category-create',
    templateUrl: './category-create.component.html',
    styleUrls: ['./category-create.component.css']
})


export class CategoryCreateComponent implements OnInit {

    data: any = {};
    list_category: any;

    constructor(protected baseService: BaseService, private router: Router) {

    }

    ngOnInit() {
        $('.dropify').dropify();
        this.baseService.methodGet('category/list').subscribe(res => {
            let rs: any = res;
            this.list_category = rs.data;
        })
    }

    create() {
        var self = this;
        var fd = new FormData($('#category_form')[0]);

        this.baseService.methodPost('category/create', fd).subscribe(
            res => {
                let rs: any = res;
                if(rs.success){
                    swal({
                        title: "GOod JObs!",
                        text: "Create Success",
                        type: "success"
                    }, function() {
                        self.router.navigate(['/category'])
                    });
                }else{
                    swal('ERorR!', 'Fails', 'danger');
                }
            },
            error => {
                alert('loi roi');
            }
        )
        
    }

    prepareAppend(fd: FormData, data) {
        for (var key in data) {
            fd.append(key, data[key]);
        }
    }
}

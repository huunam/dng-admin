import {Component, OnInit} from '@angular/core';
import {BaseComponent} from "../../../core/base.component";
import {BaseService} from "../../../core/base.service";

import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';

declare var $: any;
declare var SyntaxHighlighter: any;

@Component({
    selector: 'app-post-list',
    templateUrl: './post-list.component.html',
    styleUrls: ['./post-list.component.css']
})
export class PostListComponent extends BaseComponent implements OnInit {

    all_category: any;

    //filter
    filter: any = {
        name: '',
        category_id: 0,
        status: 'all'
    };

    //options froala
    public options: Object = {
        placeholderText: 'Edit Your Content Hwwwwwwwwwwwere!',
        charCounterCount: true,
        heightMax: 500
    };

    //search tag
    result_search_tag: any = [];
    show_box_search_tag: any = false;
    keyword_tag: any;
    modelChanged: Subject<string> = new Subject<string>();

    constructor(protected baseService: BaseService) {
        super(baseService);
        this.modelChanged
            .debounceTime(400) // wait 400ms after the last event before emitting last event
            .distinctUntilChanged() // only emit if value is different from previous value
            .subscribe(keyword => {
                this.keyword_tag = keyword;
                if(this.keyword_tag){
                    this.searchTag(this.keyword_tag);
                }else{
                    this.show_box_search_tag = false;
                }
            });
    }
    changed(text: string) {
        this.modelChanged.next(text);
    }

    ngOnInit() {

        this.baseService.methodGet('post/list').subscribe(res => {
            let rs: any = res;
            this.all_element = rs.data;
            this.total_page = Math.ceil(this.all_element.length / this.limit);
            this.list_element = this.all_element.slice(this.offset, this.offset + this.limit);
        });
        this.baseService.methodGet('category/list').subscribe(res => {
            let rs: any = res;
            this.all_category = rs.data;
        });
    }

    filterPost() {
        this.current_page = 1;
        this.offset = 0;

        let fd = new FormData();
        this.prepareAppend(fd, this.filter);

        this.baseService.methodPost('post/filter', fd).subscribe(res => {
            let rs: any = res;
            this.all_element = rs.data;
            this.total_page = Math.ceil(this.all_element.length / this.limit);
            this.list_element = this.all_element.slice(this.offset, this.offset + this.limit);
        });
    }

    savePost() {
        var fd = new FormData($('#form_update')[0]);

        this.saveElement(fd, 'post');
    }

    deletePost(item) {
        this.deleteElement(item, 'post');
    }

    searchTag(keyword) {

        this.baseService.methodGet('tag/search?keyword=' + keyword).subscribe(res => {
            let rs: any = res;
            this.result_search_tag = rs.data;

            if(this.result_search_tag.length > 0){
                this.show_box_search_tag = true;
            }else{
                this.show_box_search_tag = false;
            }
        });
    }

    cancelEdit() {
        this.edit = false;
        this.show_box_search_tag = false;
        this.keyword_tag = '';
    }

}

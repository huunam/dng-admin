import {Component, OnInit} from '@angular/core';
import {CategoryService} from '../category.service';
import {BaseComponent} from '../../../core/base.component';
import {BaseService} from "../../../core/base.service";

declare var swal: any;
declare var $: any;

@Component({
    selector: 'app-category-list',
    templateUrl: './category-list.component.html',
    styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent extends BaseComponent implements OnInit {

    constructor(protected baseService: BaseService, private categoryService: CategoryService) {
        super(baseService);
    }

    ngOnInit() {

        $('.dropify').dropify();
        
        this.categoryService.methodGet('category/list').subscribe(res => {
            let rs: any = res;
            this.all_element = rs.data;
            this.total_page = Math.ceil(this.all_element.length / this.limit);
            this.list_element = this.all_element.slice(this.offset, this.offset + this.limit);
        })
    }

    saveCategory() {

        var fd = new FormData($('#form_update')[0]);
        this.saveElement(fd, 'category');
    }

    deleteCategory(item) {
        this.deleteElement(item, 'category');
    }

}

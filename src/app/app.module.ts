import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {FormsModule} from "@angular/forms";
import {AppComponent} from './app.component';
import {RouterModule, Routes} from "@angular/router";
import {PagesModule} from "./pages/pages.module";
import {SharedModule} from "./shared/shared.module";
import {BaseService} from "./core/base.service";

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


// auth
import { AuthGuard } from './_guards/auth.guard';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { AuthenticationService } from './_services/authentication.service';
import { UserService } from './_services/user.service';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';

//fake backend
import { fakeBackendProvider } from './_helpers/fake-backend';


const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: '', component: HomeComponent, canActivate: [AuthGuard] }
];

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        HomeComponent
    ],
    imports: [
        HttpClientModule,
        BrowserModule,
        PagesModule,
        SharedModule,
        FormsModule,
        RouterModule.forRoot(routes, {useHash: true})
    ],
    providers: [
        BaseService,
        AuthGuard,
        AuthenticationService,
        UserService,
        fakeBackendProvider
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}

/*
Template Name: Admin Press Admin
Author: Themedesigner
Email: niravjoshi87@gmail.com
File: js
*/
$(function() {
    "use strict";
    // ============================================================== 
    // Sales overview
    // ============================================================== 
    Morris.Area({
        element: 'earning',
        data: [{
                period: '2011-11-01',
                Sales: 50,
                Earning: 900,
                Marketing: 20
            }, {
                period: '2011-11-02',
                Sales: 130,
                Earning: 100,
                Marketing: 80
            }, {
                period: '2011-11-03',
                Sales: 80,
                Earning: 60,
                Marketing: 70
            }, {
                period: '2011-11-04',
                Sales: 70,
                Earning: 200,
                Marketing: 140
            }, {
                period: '2011-11-05',
                Sales: 80,
                Earning: 150,
                Marketing: 140
            }, {
                period: '2011-11-06',
                Sales: 105,
                Earning: 100,
                Marketing: 80
            },
            {
                period: '2011-11-07',
                Sales: 20,
                Earning: 15,
                Marketing: 200
            },
            {
                period: '2011-11-08',
                Sales: 25,
                Earning: 50,
                Marketing: 200
            },
            {
                period: '2011-11-09',
                Sales: 250,
                Earning: 5,
                Marketing: 200
            },
            {
                period: '2011-11-10',
                Sales: 20,
                Earning: 15,
                Marketing: 200
            },
            {
                period: '2011-11-11',
                Sales: 20,
                Earning: 50,
                Marketing: 200
            },
            {
                period: '2011-11-12',
                Sales: 25,
                Earning: 10,
                Marketing: 200
            },
            {
                period: '2011-11-13',
                Sales: 250,
                Earning: 15,
                Marketing: 20
            },
            {
                period: '2011-11-14',
                Sales: 20,
                Earning: 50,
                Marketing: 20
            },
            {
                period: '2011-11-15',
                Sales: 25,
                Earning: 15,
                Marketing: 200
            },
            {
                period: '2011-11-16',
                Sales: 20,
                Earning: 10,
                Marketing: 0
            },
            {
                period: '2011-11-17',
                Sales: 0,
                Earning: 10,
                Marketing: 200
            },
            {
                period: '2011-11-18',
                Sales: 2,
                Earning: 50,
                Marketing: 200
            },
            {
                period: '2011-11-19',
                Sales: 50,
                Earning: 10,
                Marketing: 200
            },
            {
                period: '2011-11-20',
                Sales: 20,
                Earning: 10,
                Marketing: 20
            },
            {
                period: '2011-11-21',
                Sales: 250,
                Earning: 150,
                Marketing: 200
            },
            {
                period: '2011-11-22',
                Sales: 250,
                Earning: 150,
                Marketing: 200
            }
        ],
        xkey: 'period',
        ykeys: ['Sales', 'Earning'],
        labels: ['Sales', 'Earning'],
        pointSize: 3,
        fillOpacity: 0,
        pointStrokeColors: ['#1976d2', '#26c6da', '#1976d2'],
        behaveLikeLine: true,
        gridLineColor: '#e0e0e0',
        lineWidth: 3,
        hideHover: 'auto',
        lineColors: ['#1976d2', '#26c6da', '#1976d2'],
        resize: true

    });

    // ============================================================== 
    // Sales overview
    // ==============================================================
    // ============================================================== 
    // Download count
    // ============================================================== 

    var sparkResize;

    sparklineLogin();


});
/**
 * Created by phnam on 1/14/18.
 */

import {NgModule} from '@angular/core';
import {environment} from "../../environments/environment";
import {BaseService} from "./base.service";

declare var $: any;
declare var swal: any;

export class BaseComponent {
    
    //website
    protected WEBSITE = '';

    //pagination

    protected current_page = 1;
    protected offset = 0;
    protected limit = 15;
    protected total_page = 1;

    //edit
    public edit = false;
    public current_edit;

    //list
    public list_element;
    protected all_element;

    constructor(protected baseService: BaseService) {
        this.WEBSITE = environment['WEBSITE'];
    }

    editElement(item) {
        this.current_edit = item;
        this.edit = true;
    }

    cancelEdit() {
        this.edit = false;
    }


    changePage(new_number_page){
        this.current_page = new_number_page;
        this.offset = (new_number_page - 1) * this.limit;

        this.list_element = this.all_element.slice(this.offset, this.offset + this.limit);
    }

    createElement(item, type_element){
        let fd = new FormData();
        fd.append('data', JSON.stringify(item));
        var self = this;

        this.baseService.methodPost(type_element + '/new', fd).subscribe(
            res => {
                let rs: any = res;
                if(rs.success){
                    swal('Good Jobs!', 'Create ' + type_element + ' success', 'success');
                    if(type_element == 'block'){
                        self.current_edit.blocks.push(rs.data);
                    }else{
                        self.list_element.push(rs.data);
                    }
                }
            },
            error => {
                alert('loi roi');
            }
        );
    }

    saveElement(fd, type_element) {

        this.baseService.methodPost( type_element + '/edit', fd).subscribe(
            res => {
                let rs: any = res;
                if(rs.success){
                    swal('Good Jobs!', 'Edit ' + type_element + ' success', 'success');
                }
            },
            error => {
                alert('loi roi');
            }
        )
    }

    deleteElement(item, type_element) {

        var self = this;

        this.confirmDelete(function () {
            let fd = new FormData();
            fd.append('id', item.id);

            self.baseService.methodPost(type_element + '/delete', fd).subscribe(
                res => {
                    let rs: any = res;
                    if(rs.success){
                        self.list_element = self.list_element.filter(function(el){ return el.id != item.id; });
                        swal('Good Jobs!', 'Delete ' + type_element + ' success', 'success');
                        if(type_element == 'page'){
                            self.current_edit = null;
                            if(self.list_element.length > 0){
                                self.current_edit = self.list_element[0];
                            }
                        }
                        if(type_element == 'block'){
                            self.current_edit.blocks = self.current_edit.blocks.filter(function(el){ return el.id != item.id; });
                        }
                    }
                },
                error => {
                    alert('loi roi');
                }
            )
        })
    }

    confirmDelete(cb) {
        swal({
                title: "Are you sure?",
                text: "This will delete!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: true
            },
            function () {
                cb();
            }
        );
    }

    //dung vong lap for chuyen data tu json thanh FormData
    prepareAppend(fd: FormData, data) {
        for (var key in data) {
            fd.append(key, data[key]);
        }
    }

    //Tao mang dung cho vong lap for i = 1 .. n
    createRange(number){
        var items: number[] = [];
        for(var i = 1; i <= number; i++){
            items.push(i);
        }
        return items;
    }
}
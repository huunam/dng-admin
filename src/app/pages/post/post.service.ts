/**
 * Created by phnam on 1/7/18.
 */
import {Injectable} from "@angular/core";
import {BaseService} from "../../core/base.service";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class PostService extends BaseService {

	protected list_post;

    constructor(http: HttpClient) {
        super(http);
    }

    listPost(){
        return this.methodGet('post/list');
    }

    filterPost(datafilter){
        let fd = new FormData();
        this.prepareAppend(fd, datafilter);
    	return this.methodPost('post/filter', fd);
    }

    prepareAppend(fd: FormData, data) {
        for (var key in data) {
            fd.append(key, data[key]);
        }
    }
    

}
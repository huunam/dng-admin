import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import "rxjs/Rx";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {RequestOptions} from "@angular/http";

@Injectable()
export class BaseService {

    protected API_URL = '';
    protected headers: any;
    protected options;

    constructor(private http: HttpClient) {
        this.API_URL = environment['API_URL'];
        this.headers = new Headers(
            {'Accept': 'application/json'}
        );
        this.options = new RequestOptions({headers: this.headers, withCredentials: false});
    }

    getApiUrl(url, fullPath = false) {
        return this.API_URL + '/' + url;
    }

    methodGet(action) {
        return this.http.get(this.getApiUrl(action), this.options)
            .map(res => {
                return res;
            });
    }

    methodPost(action, fd: FormData) {
        return this.http.post(this.getApiUrl(action), fd, this.options)
            .map(res => {
                return res;
            });
    }
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TesteditorComponent} from './testeditor.component';
import {RouterModule, Routes} from "@angular/router";

//wysiwyg editor
import {FroalaEditorModule, FroalaViewModule} from 'angular-froala-wysiwyg';

export const routes = [
    {
        path: 'testeditor', component: TesteditorComponent
    },

];

@NgModule({
    imports: [
        FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
        CommonModule,
        RouterModule.forChild(routes)
    ],
    declarations: [TesteditorComponent]
})
export class TesteditorModule {
}
